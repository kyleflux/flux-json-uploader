'use strict';

function FluxApp (clientKey, redirectUri, projectMenu, isProd){
    this._fluxDataSelector = new FluxDataSelector(clientKey, redirectUri, {isProd:isProd});
    this._projectMenu = projectMenu;

    // Setup Flux Data Selector
    this._fluxDataSelector.setOnInitial(this.onInit.bind(this));
    this._fluxDataSelector.setOnLogin(this.onLogin.bind(this));
    this._fluxDataSelector.setOnProjects(this.populateProjects.bind(this));
    this._fluxDataSelector.setOnKeys(function () {});
    this._fluxDataSelector.setOnValue(function () {});
    this._fluxDataSelector.init();
}

FluxApp.prototype.login = function () {
    this._fluxDataSelector.login();
}

FluxApp.prototype.onInit = function () {
}

FluxApp.prototype.onLogin = function () {
    this._fluxDataSelector.showProjects();

}

FluxApp.prototype.selectProject = function () {
    this._fluxDataSelector.selectProject(this._projectMenu.value);
    this._dt = this._fluxDataSelector.getDataTable(this._projectMenu.value).table;
}

FluxApp.prototype.createKey = function (name, json) {
    this._dt.createCell(name, {value:json}).then(function (cell) {
        console.log(cell);
    });
}

FluxApp.prototype.populateProjects = function (projectPromise) {
    var _this = this;
    projectPromise.then(function (projects) {
        for (var i=projects.entities.length-1;i>=0;i--) {
            var entity = projects.entities[i];
            var option = document.createElement('option');
            _this._projectMenu.appendChild(option);
            option.value = entity.id;
            option.textContent = entity.name;
        }
    });
}

FluxApp.prototype.logout = function () {
    this._fluxDataSelector.logout();
}

/**
 * Gets the flux token from it's place in cookies or localStorage.
 */
FluxApp.prototype.getFluxToken = function () {
    var fluxCredentials = JSON.parse(localStorage.getItem('fluxCredentials'));
    return fluxCredentials.fluxToken;
}

FluxApp.prototype.jsonFileChanged = function (selector) {
    // file name map
    var files = selector.files;
    for (var i=0;i<files.length; i++) {
        var file = selector.files[i];
        if (file.name.indexOf('.json')!==-1) {
            this._readJsonFile(file);
        }
    }
};


FluxApp.prototype._readJsonFile = function (jsonFile) {
    var reader = new FileReader();
    reader.onloadend = this.getTextFileReadHandler(jsonFile);
    reader.readAsText(jsonFile);
}

FluxApp.prototype.getTextFileReadHandler = function (jsonFile) {
    var fileNameBase = FluxApp.stripExtension(jsonFile.name, 'json');
    var _this = this;
    return function () {
        var result = event.target.result;
        // Check if the input is viewport state or otherwise just flux entities
        var json = JSON.parse(result);
        _this.createKey(fileNameBase, json);
    }
};

FluxApp.stripExtension = function (fileName, extension) {
    var extensionDot = '.'+extension;
    return fileName.substring(0,fileName.length-extensionDot.length);
}
